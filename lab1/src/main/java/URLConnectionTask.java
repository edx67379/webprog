import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionTask {
    public static void main(String[] args) {
        String urlString = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Enter URL: ");
            urlString = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            URL url = new URL(urlString);
            URLConnection urlCon = url.openConnection();
            InputStream is = urlCon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("body.txt"));
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, bytesRead);
            }
            System.out.println(urlCon.getHeaderFields());
            bos.close();
            bis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
