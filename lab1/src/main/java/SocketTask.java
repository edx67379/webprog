import java.io.*;
import java.net.Socket;
import java.net.URL;

public class SocketTask {
    public static void getRequest(BufferedWriter out, String host, String path) throws IOException {
        out.write("GET " + path + " HTTP/1.1");
        out.newLine();
        out.write("Host: " + host);
        out.newLine();
        out.write("Connection: Close");
        out.newLine();
        out.newLine();
        out.flush();
    }
    public static void main(String[] args) {
        String urlString = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            urlString = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            URL url = new URL(urlString);
            String host = url.getHost();
            String path = url.getPath();
            System.out.println(path);

            System.out.println("URL: " + host);
            Socket socket = new Socket(host, 80);
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("body.txt")));

            getRequest(out, host, path);

            String line = in.readLine();
            while (line != null && !line.equals("")) {
                System.out.println(line);
                line = in.readLine();
            }
            while (line != null) {
                file.write(line);
                file.newLine();
                line = in.readLine();
            }
            in.close();
            out.close();
            file.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
